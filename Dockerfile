FROM node:14-alpine

WORKDIR /cards-game-nguyenhuulam

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn build

EXPOSE 8080

CMD [ "yarn", "start:prod" ]
