import { NODE_APP_PORT } from './configs/configs';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './common/utils/http-exception-filter';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const logger = new Logger('bootstrap');
  const app = await NestFactory.create(AppModule);

  // Swagger configs
  const swaggerConfigs = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Cards Game - Nguyen Huu Lam')
    .setDescription('Cards Game API Documentation.')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfigs);

  SwaggerModule.setup('api-docs', app, document, {
    swaggerOptions: {
      displayRequestDuration: true,
      filter: true,
    },
  });

  // Use global filters
  app.useLogger(logger);
  app.useGlobalFilters(new HttpExceptionFilter());

  // Enable CORS for AWS.
  app.enableCors();

  await app.listen(NODE_APP_PORT, () => {
    logger.log(`Application running on port ${NODE_APP_PORT}.`);
  });
}
bootstrap();
