import * as dotenv from 'dotenv';

dotenv.config();

export const getEnv = (key: string): string => {
  const value = process.env[key];
  return value;
};

export const NODE_APP_PORT = getEnv('NODE_APP_PORT') || 3000;

export const MONGO_USERNAME = getEnv('MONGO_USERNAME');
export const MONGO_PASSWORD = getEnv('MONGO_PASSWORD');
export const MONGO_HOST = getEnv('MONGO_HOST');
export const MONGO_DATABASE = getEnv('MONGO_DATABASE');

export const JWT_SECRET_KEY = getEnv('JWT_SECRET_KEY');
export const JWT_EXPIRES_IN = Number(getEnv('JWT_EXPIRES_IN'));

export const DATABASE_URI = `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}/${MONGO_DATABASE}`;
