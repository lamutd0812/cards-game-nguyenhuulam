import { JwtPayloadDto } from './dto/jwt-payload.dto';
import { JWT_SECRET_KEY, JWT_EXPIRES_IN } from './../../configs/configs';
import { SignInResponseDto } from './dto/sign-in-response.dto';
import { InjectModel } from '@nestjs/mongoose';
import { SignUpResponseDto } from './dto/sign-up-response.dto';
import { ErrorMessages as AuthErrorMessages, Messages as AuthMessages } from './auth.constant';
import { AuthDto } from './dto/auth.dto';
import { User, UsersDocument, USERS_MODEL } from './../users/users.schema';
import { BadRequestException, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(USERS_MODEL)
    private readonly usersModel: Model<UsersDocument>,
  ) {}

  private hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  private async validatePassword(
    password: string,
    passwordDb: string,
    salt: string,
  ): Promise<boolean> {
    const hashedPassword = await bcrypt.hash(password, salt);
    return hashedPassword === passwordDb;
  }

  verifyToken(token: string) {
    const verifiedToken = jwt.verify(token, JWT_SECRET_KEY);
    return verifiedToken;
  }

  generateToken(userId: string, email: string): string {
    const payload = { userId, email };

    return jwt.sign(payload, JWT_SECRET_KEY, {
      expiresIn: JWT_EXPIRES_IN,
    });
  }

  async signInValidate(signInDto: AuthDto) {
    const { email, password } = signInDto;

    const user = await this.usersModel.findOne({ email });
    if (!user) {
      throw new UnauthorizedException(AuthErrorMessages.USERNAME_OR_PASSWORD_INCORRECT);
    }
    const { password: passwordDb, salt } = user;

    const isPasswordCorrect = await this.validatePassword(password, passwordDb, salt);

    if (!isPasswordCorrect) {
      throw new UnauthorizedException(AuthErrorMessages.USERNAME_OR_PASSWORD_INCORRECT);
    }

    return user;
  }

  async signUp(signUpDto: AuthDto): Promise<SignUpResponseDto> {
    const { email, password } = signUpDto;

    const user = await this.usersModel.findOne({email});
    if (user) {
      throw new BadRequestException(AuthErrorMessages.EMAIL_ALREADY_EXISTS);
    }

    const salt = await bcrypt.genSalt();
    const hashedPassword = await this.hashPassword(password, salt);

    const newUser = await this.usersModel.create({
      email,
      password: hashedPassword,
      salt,
    });

    return {
      statusCode: HttpStatus.CREATED,
      success: true,
      data: { email: newUser.email },
      message: AuthMessages.SIGN_UP_SUCCESSFULLY,
    };
  }

  async signIn(signInDto: AuthDto): Promise<SignInResponseDto> {
    const { email } = signInDto;

    const user = await this.signInValidate(signInDto);
    const { _id: userId, totalCoins } = user;

    const accessToken = this.generateToken(userId, email);
    const jwtObject: JwtPayloadDto = {
      userId,
      email,
      totalCoins,
      accessToken,
    };

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: jwtObject,
      message: AuthMessages.SIGN_IN_SUCCESSFULLY,
    };
  }

  verifyAccessToken(token: string) {
    const verifiedToken = jwt.verify(token, JWT_SECRET_KEY);

    return verifiedToken;
  }

  async decodeAccessToken(token: string) {
    const decodedToken: any = this.verifyAccessToken(token);

    if (!decodedToken) {
      throw new UnauthorizedException(AuthErrorMessages.UNAUTHORIZED);
    }

    const authenticatedUser = await this.usersModel.findOne({
      _id: decodedToken?.userId,
      email: decodedToken?.email,
    });
    if (!authenticatedUser) {
      throw new UnauthorizedException(AuthErrorMessages.USER_NOT_FOUND);
    }

    return authenticatedUser;
  }
}
