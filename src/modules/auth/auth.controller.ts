import { SignInResponseDto } from './dto/sign-in-response.dto';
import { SignUpResponseDto } from './dto/sign-up-response.dto';
import { AuthService } from './auth.service';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiOperation, ApiTags, ApiOkResponse, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Body, Controller, Post, ValidationPipe } from '@nestjs/common';
import { AuthDto } from './dto/auth.dto';
import { ErrorMessages as AuthErrorMessages } from './auth.constant';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post('/signup')
  @ApiOperation({ summary: 'Signup' })
  @ApiCreatedResponse({ type: SignUpResponseDto })
  @ApiBadRequestResponse({ description: AuthErrorMessages.EMAIL_ALREADY_EXISTS })
  signUp(
    @Body(ValidationPipe) signUpDto: AuthDto,
  ): Promise<SignUpResponseDto> {
    return this.authService.signUp(signUpDto);
  }

  @Post('/signin')
  @ApiOperation({ summary: 'Signin' })
  @ApiOkResponse({ type: SignInResponseDto })
  @ApiUnauthorizedResponse({ description: AuthErrorMessages.USERNAME_OR_PASSWORD_INCORRECT })
  signIn(
    @Body(ValidationPipe) signInDto: AuthDto,
  ): Promise<SignInResponseDto> {
    return this.authService.signIn(signInDto);
  }
}
