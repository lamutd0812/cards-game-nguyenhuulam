export enum ErrorMessages {
  EMAIL_INVALID = 'Email invalid.',
  EMAIL_ALREADY_EXISTS = 'Email already exists',
  USER_NOT_FOUND = 'User not found.',
  USERNAME_OR_PASSWORD_INCORRECT = 'Username or password incorrect.',
  UNAUTHORIZED = 'Unauthorized.',
}

export enum Messages {
  SIGN_UP_SUCCESSFULLY = 'Create account successfully.',
  SIGN_IN_SUCCESSFULLY = 'Signin successfully.',
}