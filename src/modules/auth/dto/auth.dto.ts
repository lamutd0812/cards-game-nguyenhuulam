import { ErrorMessages as AuthErrorMessages } from '../auth.constant';
import { PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH } from '../../users/users.schema';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';

export class AuthDto {
  @ApiProperty({ example: 'lamtest@gmail.com' })
  @IsNotEmpty()
  @IsString()
  @MinLength(10)
  @Matches(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    { message: AuthErrorMessages.EMAIL_INVALID },
  )
  email: string;

  @ApiProperty({ example: 'Lam@123456789' })
  @IsNotEmpty()
  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  @MaxLength(PASSWORD_MAX_LENGTH)
  password: string;
}
