import { ApiProperty } from '@nestjs/swagger';
import { CommonResponseDto } from './../../../common/dto/common-response.dto';

class SignUpSuccessData {
  @ApiProperty()
  email: string;
}

export class SignUpResponseDto extends CommonResponseDto {
  @ApiProperty()
  data: SignUpSuccessData;
}