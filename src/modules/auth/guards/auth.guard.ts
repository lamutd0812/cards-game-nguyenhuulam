import { AuthService } from './../auth.service';
import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { ErrorMessages as AuthErrorMessages } from '../auth.constant';

@Injectable()
export class LocalAuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
  ){}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      const authHeader = context.switchToHttp().getRequest()
        .headers.authorization;
      const accessToken = authHeader.replace('Bearer ', '');

      const authenticatedUser = await this.authService.decodeAccessToken(
        accessToken,
      );

      if (!authenticatedUser) {
        throw new UnauthorizedException(AuthErrorMessages.UNAUTHORIZED);
      }

      const user = {
        userId: authenticatedUser._id,
        email: authenticatedUser.email,
      };

      context.switchToHttp().getRequest().user = user;

      return true;
    } catch (error) {
      throw new UnauthorizedException(AuthErrorMessages.UNAUTHORIZED);
    }
  }
}