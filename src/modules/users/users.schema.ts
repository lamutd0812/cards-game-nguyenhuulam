import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsString } from 'class-validator';
import { Document, Schema } from 'mongoose';

export const USERS_MODEL = 'users';
export const MIN_COINS = 0;
export const MAX_COINS = 10;
export const PASSWORD_MIN_LENGTH = 6;
export const PASSWORD_MAX_LENGTH = 15;

export const UsersSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
      minlength: PASSWORD_MIN_LENGTH,
    },
    salt: {
      type: String,
      required: true,
    },
    totalCoins: {
      type: Number,
      required: true,
      min: MIN_COINS,
      max: MAX_COINS,
      default: MAX_COINS,
    },
    inWhiteList: {
      type: Boolean,
      required: true,
      default: true,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    collection: USERS_MODEL,
  },
);

export class User {
  userId: string;

  @ApiProperty()
  @IsString()
  email: string;

  @ApiProperty()
  @IsString()
  password: string;

  @ApiProperty()
  @IsString()
  salt: string;

  @ApiProperty()
  @IsNumber()
  totalCoins: number;

  @ApiProperty()
  @IsBoolean()
  inWhiteList: boolean;
}

export interface UsersDocument extends User, Document {}
