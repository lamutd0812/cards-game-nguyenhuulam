import { ApiTags } from '@nestjs/swagger';
import { USERS_MODEL } from './users.schema';
import { UsersService } from './users.service';
import { Controller, Get, UseGuards } from '@nestjs/common';
import { Authorization } from 'src/common/decorators/auth.decorator';

@Controller('users')
@ApiTags(USERS_MODEL)
export class UsersController {}
