import { CommonResponseDto } from 'src/common/dto/common-response.dto';
import { ApiProperty } from '@nestjs/swagger';
import { BuyCardsDto } from './buy-cards.dto';

export class BuyCardsResponseDto extends CommonResponseDto {
  @ApiProperty()
  data: BuyCardsDto;
}
