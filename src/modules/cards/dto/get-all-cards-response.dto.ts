import { ApiProperty } from '@nestjs/swagger';
import { Card } from '../schemas/cards.schema';
import { CommonResponseDto } from './../../../common/dto/common-response.dto';

export class CardsResponseDto extends CommonResponseDto {
  @ApiProperty({ type: [Card]})
  data: Card[];
}