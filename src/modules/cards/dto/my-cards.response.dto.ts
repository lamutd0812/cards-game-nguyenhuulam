import { CommonResponseDto } from 'src/common/dto/common-response.dto';
import { ApiProperty } from '@nestjs/swagger';
import { BuyCardsDto } from './buy-cards.dto';
import { UserCard } from '../schemas/user-cards.schema';
import { Card } from '../schemas/cards.schema';

export class UserCardsResponse extends UserCard{
  @ApiProperty({ type: Card })
  cardData: Card;
}

export class MyCardsResponseDto extends CommonResponseDto {
  @ApiProperty({ type: [UserCardsResponse]})
  data: UserCardsResponse[];
}
