import { AuthDto } from './../../auth/dto/auth.dto';
import { ApiProperty } from '@nestjs/swagger';
import { BuyCardsDto } from './buy-cards.dto';

export class UpdateCardsDto extends AuthDto{
  @ApiProperty({ type: [BuyCardsDto]})
  cards: BuyCardsDto[];
}