import { ApiProperty } from '@nestjs/swagger';
import { CommonResponseDto } from './../../../common/dto/common-response.dto';
import { ReturnCardsDto } from './return-cards.dto';

export class ReturnCardsResponseDto extends CommonResponseDto {
  @ApiProperty()
  data: ReturnCardsDto;
}