import { CardsResponseDto } from './dto/get-all-cards-response.dto';
import { CardErrorMessages } from './cards.constant';
import { ReturnCardsDto } from './dto/return-cards.dto';
import { ReturnCardsResponseDto } from './dto/return-cards-response.dto';
import { ApiTags, ApiOperation, ApiOkResponse, ApiNotFoundResponse } from '@nestjs/swagger';
import { CardsService } from './cards.service';
import { Body, Controller, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { CARDS_MODEL } from './schemas/cards.schema';
import { Authorization } from 'src/common/decorators/auth.decorator';
import { ReqUser } from 'src/common/decorators/user.decorator';
import { User } from '../users/users.schema';
import { BuyCardsDto } from './dto/buy-cards.dto';
import { BuyCardsResponseDto } from './dto/buy-cards-response.dto';
import { MyCardsResponseDto } from './dto/my-cards.response.dto';
import { UpdateCardsDto } from './dto/update-cards.dto';

@Controller('cards')
@ApiTags(CARDS_MODEL)
export class CardsController {
  constructor(
    private readonly cardsService: CardsService,
  ){}

  // @Post('/init-cards')
  // @ApiOperation({ summary: 'Init cards' })
  // @Authorization()
  // createSampleUser(): Promise<any> {
  //   return this.cardsService.initCard();
  // }

  @Get()
  @ApiOperation({ summary: 'Get all cards' })
  @ApiOkResponse({ type: CardsResponseDto })
  getAllCards(): Promise<CardsResponseDto> {
    return this.cardsService.getAllCards();
  }

  @Get('/my-cards')
  @ApiOperation({ summary: 'Get bought cards' })
  @Authorization()
  @ApiOkResponse({ type: MyCardsResponseDto })
  getMyCards(@ReqUser() user: User): Promise<MyCardsResponseDto> {
    return this.cardsService.getMyCards(user);
  }

  @Get('/see-cards/:userEmail')
  @ApiOperation({ summary: 'See cards' })
  @ApiOkResponse({ type: MyCardsResponseDto })
  @ApiNotFoundResponse({ description: CardErrorMessages.USER_HAS_NO_CARDS})
  seeCards(@Param('userEmail') userEmail: string): Promise<MyCardsResponseDto> {
    return this.cardsService.seeCards(userEmail);
  }

  @Post('/buy-cards')
  @ApiOperation({ summary: 'Buy cards' })
  @Authorization()
  @ApiOkResponse({ type: BuyCardsResponseDto })
  buyCards(
    @ReqUser() user: User,
    @Body() buyCardsDto: BuyCardsDto,
  ): Promise<BuyCardsResponseDto> {
    return this.cardsService.buyCards(user, buyCardsDto);
  }

  @Put('/return-cards')
  @ApiOperation({ summary: 'Return cards' })
  @Authorization()
  @ApiOkResponse({ type: ReturnCardsResponseDto })
  returnCards(
    @ReqUser() user: User,
    @Body() returnCardsDto: ReturnCardsDto,
  ): Promise<BuyCardsResponseDto> {
    return this.cardsService.returnCards(user, returnCardsDto);
  }

  @Put('/update-cards')
  @ApiOperation({ summary: 'Update cards' })
  updateUserCards(@Body() updateCardsDto: UpdateCardsDto): Promise<any> {
    return this.cardsService.updateUserCards(updateCardsDto);
  }
}
