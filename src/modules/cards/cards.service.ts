import { CardsResponseDto } from './dto/get-all-cards-response.dto';
import { AuthService } from './../auth/auth.service';
import { ReturnCardsResponseDto } from './dto/return-cards-response.dto';
import { ReturnCardsDto } from './dto/return-cards.dto';
import { UserCardsDocument, USER_CARDS_MODEL, UserCard } from './schemas/user-cards.schema';
import { USERS_MODEL, UsersDocument, MAX_COINS } from './../users/users.schema';
import { CARD_TYPES_MODEL, CardTypesDocument } from './schemas/card-types.schema';
import { ElementType, CardMessages, CardErrorMessages } from './cards.constant';
import { Injectable, HttpStatus, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CARDS_MODEL, CardsDocument, Card } from './schemas/cards.schema';
import { User } from '../users/users.schema';
import { BuyCardsDto } from './dto/buy-cards.dto';
import { BuyCardsResponseDto } from './dto/buy-cards-response.dto';
import { MyCardsResponseDto, UserCardsResponse } from './dto/my-cards.response.dto';
import { UpdateCardsDto } from './dto/update-cards.dto';
@Injectable()
export class CardsService {
  constructor(
    @InjectModel(CARDS_MODEL)
    private readonly cardsModel: Model<CardsDocument>,
    @InjectModel(USER_CARDS_MODEL)
    private readonly userCardsModel: Model<UserCardsDocument>,
    @InjectModel(CARD_TYPES_MODEL)
    private readonly cardTypesModel: Model<CardTypesDocument>,
    @InjectModel(USERS_MODEL)
    private readonly usersModel: Model<UsersDocument>,
    private readonly authService: AuthService,
  ){}

  async initCard(): Promise<any> {
    const cardTypeData = [
      { name: ElementType.EARTH, power: 1 },
      { name: ElementType.WOOD, power: 2 },
      { name: ElementType.FIRE, power: 3 },
      { name: ElementType.WATER, power: 4 },
    ];

    const cardTypes = await this.cardTypesModel.insertMany(cardTypeData);

    const cardsData = [];
    for (let i = 0; i < cardTypes.length; i++) {
      const cardType = cardTypes[i];
      for (let j = 1; j <= 5; j++) {
        cardsData.push({
          name: `${cardType.name} ${j}`,
          power: j,
          price: j,
          elementTypeId: cardType._id,
        });
      }
    }

    const cards = await this.cardsModel.insertMany(cardsData);
    return cards;
  }

  async buyCards(reqUser: User, buyCardsDto: BuyCardsDto): Promise<BuyCardsResponseDto> {
    const { userId } = reqUser;
    const { cardId, quantity } = buyCardsDto;

    const card = await this.cardsModel.findById(cardId);
    if (!card) {
      throw new NotFoundException(CardErrorMessages.CARD_NOT_FOUND);
    }

    const user = await this.usersModel
      .findById(userId)
      .select('totalCoins inWhiteList');

    const { totalCoins } = user;
    const { price } = card;
    const totalCost = price * quantity;
    if (totalCoins - totalCost < 0) {
      throw new BadRequestException(CardErrorMessages.COINS_NOT_ENOUGH);
    }

    const userCardsUpdate = await this.userCardsModel.findOneAndUpdate(
      { cardId, userId },
      {
        $inc: { quantity },
      },
      { new: true, upsert: true },
    );
    if (userCardsUpdate) {
      await this.usersModel.findByIdAndUpdate(userId, {
        $inc: { totalCoins: -totalCost },
      }, { new: true });
    }

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: {
        cardId,
        quantity,
      },
      message: CardMessages.BUY_CARDS_SUCCESSFULLY,
    };
  }

  async returnCards(reqUser: User, returnCardsDto: ReturnCardsDto): Promise<ReturnCardsResponseDto> {
    const { userId } = reqUser;
    const { cardId, quantity } = returnCardsDto;

    const userCard: UserCardsResponse = await this.userCardsModel.findOne({
      userId,
      cardId,
      quantity: { $gt: 0 },
    }).populate('cardData');
    if (!userCard) {
      throw new NotFoundException(CardErrorMessages.USER_CARD_NOT_FOUND);
    }
    const {
      quantity: curQuantity,
      cardData: { price },
    } = userCard;
    if (quantity > curQuantity) {
      throw new BadRequestException(CardErrorMessages.CARDS_NOT_ENOUGH_TO_RETURN);
    }

    const totalReturnCoins = price * quantity;

    const userCardsUpdate = await this.userCardsModel.findOneAndUpdate(
      { cardId, userId },
      {
        $inc: { quantity: -quantity },
      },
      { new: true, upsert: true },
    );
    if (userCardsUpdate) {
      await this.usersModel.findByIdAndUpdate(userId, {
        $inc: { totalCoins: totalReturnCoins },
      }, { new: true });
    }

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: {
        cardId,
        quantity,
      },
      message: CardMessages.RETURN_CARDS_SUCCESSFULLY,
    };
  }

  async getAllCards(): Promise<CardsResponseDto> {
    const cards = await this.cardsModel
      .find()
      .sort('name')
      .select('-createdAt -updatedAt');

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: cards,
      message: CardMessages.GET_ALL_CARDS_SUCCESSFULLY,
    };
  }

  async getMyCards(reqUser: User): Promise<MyCardsResponseDto> {
    const { userId } = reqUser;

    const userCards: UserCardsResponse[] = await this.userCardsModel
      .find({
        userId,
        quantity: { $gt: 0 },
      })
      .select('-createdAt -updatedAt')
      .populate('cardData', '-createdAt -updatedAt');

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: userCards,
      message: CardMessages.GET_MY_CARDS_SUCCESSFULLY,
    };
  }

  async seeCards(email: string): Promise<MyCardsResponseDto> {
    const user = await this.usersModel
      .findOne({ email });
    if (!user) {
      throw new NotFoundException(CardErrorMessages.USER_NOT_FOUND);
    }

    const userCards: UserCardsResponse[] = await this.userCardsModel
      .find({ userId: user._id })
      .select('-createdAt -updatedAt')
      .populate('cardData', '-createdAt -updatedAt');

    if (!userCards.length) {
      throw new NotFoundException(CardErrorMessages.USER_HAS_NO_CARDS);
    }

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: userCards,
      message: CardMessages.GET_USER_CARDS_SUCCESSFULLY,
    };
  }

  getNewUserCardsData (userId: string, cards: BuyCardsDto[]) {
    const result: UserCard[] = cards.map(item => {
      return {...item, userId};
    });

    return result;
  }

  async updateUserCards(updateCardsDto: UpdateCardsDto): Promise<any> {
    const { email, password, cards } = updateCardsDto;
    const user = await this.authService.signInValidate({ email, password });
    const { _id: userId } = user;
    const idCardList = cards.map(card => card.cardId);
    const cardsData = await this.cardsModel.find({
      _id: { $in: idCardList },
    });
    if (cardsData.length !== idCardList.length) {
      throw new NotFoundException(CardErrorMessages.SOME_CARDS_NOT_FOUND);
    }

    let totalCoins = MAX_COINS;
    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      const { cardId, quantity } = card;
      for (let j = 0; j < cardsData.length; j++) {
        const cardData = cardsData[j];
        const { _id: cardDataId, price } = cardData;
        if (cardId === cardDataId.toString()) {
          totalCoins -= price * quantity;
        }
      }
    }
    if (totalCoins < 0) {
      throw new BadRequestException(CardErrorMessages.COINS_NOT_ENOUGH);
    }
    await this.userCardsModel.deleteMany({ userId });
    const newData = this.getNewUserCardsData(userId.toString(), cards);
    const newUserCards = await this.userCardsModel.insertMany(newData);
    if (newUserCards) {
      await user.update({ totalCoins });
    }

    return {
      statusCode: HttpStatus.OK,
      success: true,
      data: newUserCards,
      message: CardMessages.GET_USER_CARDS_SUCCESSFULLY,
    };
  }
}
