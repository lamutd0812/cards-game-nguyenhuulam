import { USERS_MODEL } from '../../users/users.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { Document, Schema } from 'mongoose';
import { Type } from 'class-transformer';
import { CARD_TYPES_MODEL } from './card-types.schema';

export const CARDS_MODEL = 'cards';
export const MIN_CARD_POWER = 1;
export const MAX_CARD_POWER = 5;
export const MIN_CARD_PRICE = 1;
export const MAX_CARD_PRICE = 5;

export const CardsSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      default: null,
    },
    power: {
      type: Number,
      required: true,
      min: MIN_CARD_POWER,
      max: MAX_CARD_POWER,
    },
    price: {
      type: Number,
      required: true,
      min: MIN_CARD_PRICE,
      max: MAX_CARD_PRICE,
    },
    elementTypeId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: CARD_TYPES_MODEL,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    collection: CARDS_MODEL,
  },
);

export class Card {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  image: string;

  @ApiProperty()
  @IsNumber()
  power: number;

  @ApiProperty()
  @IsNumber()
  price: number;

  @ApiProperty()
  @IsString()
  @Type(() => String)
  elementTypeId: string;
}

export interface CardsDocument extends Card, Document {}
