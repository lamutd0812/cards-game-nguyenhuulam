import { CARDS_MODEL } from './cards.schema';
import { USERS_MODEL } from '../../users/users.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { Document, Schema } from 'mongoose';
import { Type } from 'class-transformer';

export const USER_CARDS_MODEL = 'user_cards';

export const UserCardsSchema = new Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    cardId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
      min: 0,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    collection: USER_CARDS_MODEL,
  },
);

UserCardsSchema.virtual('cardData', {
  ref: CARDS_MODEL,
  localField: 'cardId',
  foreignField: '_id',
  justOne: true,
});

UserCardsSchema.virtual('userData', {
  ref: USERS_MODEL,
  localField: 'userId',
  foreignField: '_id',
  justOne: true,
});

export class UserCard {
  @ApiProperty()
  @IsString()
  @Type(() => String)
  userId: string;

  @ApiProperty()
  @IsString()
  @Type(() => String)
  cardId: string;

  @ApiProperty()
  @IsNumber()
  quantity: number;
}

export interface UserCardsDocument extends UserCard, Document {}
