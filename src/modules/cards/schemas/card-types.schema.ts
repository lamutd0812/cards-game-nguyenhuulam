import { ElementType } from './../cards.constant';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { Document, Schema } from 'mongoose';

export const CARD_TYPES_MODEL = 'card_types';
export const MIN_CARD_TYPE_POWER = 1;
export const MAX_CARD_TYPE_POWER = 4;

export const CardTypesSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      enum: Object.values(ElementType),
    },
    power: {
      type: Number,
      required: true,
      min: MIN_CARD_TYPE_POWER,
      max: MAX_CARD_TYPE_POWER,
    },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    collection: CARD_TYPES_MODEL,
  },
);

export class CardType {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNumber()
  power: number;
}

export interface CardTypesDocument extends CardType, Document {}
