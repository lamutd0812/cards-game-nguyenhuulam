import { AuthModule } from './../auth/auth.module';
import { CardsSchema, CARDS_MODEL } from './schemas/cards.schema';
import { CARD_TYPES_MODEL, CardTypesSchema } from './schemas/card-types.schema';
import { USER_CARDS_MODEL, UserCardsSchema } from './schemas/user-cards.schema';
import { USERS_MODEL, UsersSchema } from './../users/users.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: CARDS_MODEL, schema: CardsSchema },
      { name: USER_CARDS_MODEL, schema: UserCardsSchema },
      { name: CARD_TYPES_MODEL, schema: CardTypesSchema },
      { name: USERS_MODEL, schema: UsersSchema },
    ]),
    AuthModule,
  ],
  controllers: [CardsController],
  providers: [CardsService],
})
export class CardsModule {}
