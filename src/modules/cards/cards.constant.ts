export enum ElementType {
  WATER = 'Water',
  FIRE = 'Fire',
  WOOD = 'Wood',
  EARTH = 'Earth',
}

export enum CardMessages {
  GET_ALL_CARDS_SUCCESSFULLY = 'Get all cards successfully.',
  BUY_CARDS_SUCCESSFULLY = 'Buy cards successfully.',
  GET_MY_CARDS_SUCCESSFULLY = 'Get my cards successfully.',
  RETURN_CARDS_SUCCESSFULLY = 'Return cards successfully.',
  GET_USER_CARDS_SUCCESSFULLY = 'Get list cards of user successfully.',
}

export enum CardErrorMessages {
  CARD_NOT_FOUND = 'Card not found.',
  COINS_NOT_ENOUGH = 'Coins not enough to buy cards.',
  USER_CARD_NOT_FOUND = 'User card not found.',
  CARDS_NOT_ENOUGH_TO_RETURN = 'Cards quantity not enough to return.',
  USER_HAS_NO_CARDS = 'User cards not found.',
  USER_NOT_FOUND = 'User not found',
  SOME_CARDS_NOT_FOUND = 'Some cards not found.',
}