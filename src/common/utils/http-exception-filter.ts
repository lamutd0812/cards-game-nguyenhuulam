import { HttpExceptionDto } from './../dto/http-exception.dto';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { DayJS } from './dayjs';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {

  catch(exception: HttpException, host: ArgumentsHost) {
    console.log(exception);

    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    let status: number;
    let message: string;
    if (exception instanceof HttpException) {
      status = exception.getStatus();

      const exceptionString = JSON.stringify(exception.getResponse());
      const { message: errMessage } = JSON.parse(exceptionString);
      message = errMessage;
    } else {
      status = HttpStatus.INTERNAL_SERVER_ERROR;
      message = 'INTERNAL_SERVER_ERROR';
    }

    const timestamp = DayJS().utc().format();

    const errorResponse: HttpExceptionDto = {
      method: request.method,
      path: request.url,
      statusCode: status,
      message,
      timestamp,
    };

    return response.status(status).json(errorResponse);
  }
}
