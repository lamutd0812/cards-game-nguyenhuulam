import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from 'src/modules/users/users.schema';

export const ReqUser = createParamDecorator(
  (data: keyof User, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest();
    const user = req.user;

    return data ? user?.[data] : user;
  },
);
