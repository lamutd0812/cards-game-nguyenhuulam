import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { LocalAuthGuard } from 'src/modules/auth/guards/auth.guard';

export const Authorization = () =>
  applyDecorators(UseGuards(LocalAuthGuard), ApiBearerAuth());
